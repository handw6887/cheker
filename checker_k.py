from tkinter import Tk,Button,PhotoImage
import os, threading, copy, time, json
from ck_history import History

# from ck_socket

# 통신 모듈 달기
# 소켓 통신, 파이어베이스 둘 중 하나 선택하도록
# 대전자 둘 중 한 명이 서버, 다중접속(관전자)
# 공유해야 하는 데이터 : 

path = os.getcwd()

root=Tk()

def get_json(key_list, data_list):
    return json.dumps({ele[0] : ele[1] for ele in zip(key_list, data_list)})

def get_data_list(key_list):
    data_list = []
    for key in key_list:
        data_list.append(getattr(CheckButton, key))
    return data_list

class CheckButton(Button):
    var_names = ['grid_dict', 'turn', 'ckd', 'ckd_pt', 'overd', 'overd_pt', 'catch_pt', 'catch_again', 'king_ckd']
    image_dict = {}
    button_list = []
    white_grid = []
    black_grid = []
    # grid_dict = {}    # 밑에서 클래스 메소드로 grid_dict 속성을 만드는 상황이라서 미리 안 만들어놔도 된다.
    turn = 0 #누구의 턴인지. 0:화이트, 1:블랙
    ckd = False
    ckd_pt = (-1, -1)
    overd = False
    overd_pt = (-1, -1)
    catch_pt = tuple()
    catch_again = ''
    king_ckd = ''     # history에 포함시키기
    # 클래스 속성 자리에는 "클래스 이름.~" 쓰면 안되나요?

    def __init__(self, master=None, wbb=None, ckd=False, cnf={}, **kw):
        super().__init__(master, cnf, **kw)
        self.wbb=wbb #돌의 색깔 0:화이트, 1:블랙, 2:빈칸
        self.ckd=ckd #돌이 클릭되었는지. True:클릭상태, False:클릭안된상태, 빈칸:항상 False
        self.overd = False
        self.bind("<Enter>",self.over)
        self.bind("<Leave>",self.out)
        self.is_king = False
        self.had_catch = ''

    def click(self):
        if self.wbb !=2 and self.wbb != CheckButton.turn:
            return
        if self.wbb == 2 and not CheckButton.ckd:  #????? not?? -> 알겠음.
            return
        if CheckButton.king_ckd and CheckButton.overd:
            self.king_click()
            return 
        if CheckButton.ckd and self.overd:
            #print(CheckButton.ckd_pt, CheckButton.overd_pt) #클릭된 돌이 있고 나자신이 오버 상태일때  >>
            #이동을 하겠다. 클릭 포인트, 오버 포인트, 클릭드, 오버드, 돌의 위치가 담긴 그리드, 턴 넘기기.
            #상대편 말을 잡는 작업
            if CheckButton.catch_pt:
                catch_grid = CheckButton.button_list[CheckButton.catch_pt[0]][CheckButton.catch_pt[1]]
                middle = (int((CheckButton.ckd_pt[0]+CheckButton.overd_pt[0])/2),int((CheckButton.ckd_pt[1]+CheckButton.overd_pt[1])/2))
                if middle == CheckButton.catch_pt:
                    if catch_grid.is_king:
                        opp_king_turn = 4 if CheckButton.turn ==0 else 3
                        catch_grid.is_king = ''
                        CheckButton.grid_dict[opp_king_turn].remove(middle)
                        print(CheckButton.grid_dict)
                    catch_grid['image'] = CheckButton.image_dict[2]['bg'][catch_grid.get_bg_num()] 
                    #잡힌 말 있던 자리 초기화
                    CheckButton.grid_dict[catch_grid.wbb].remove(CheckButton.catch_pt)
                    CheckButton.button_list[CheckButton.catch_pt[0]][CheckButton.catch_pt[1]].wbb = 2
                    CheckButton.catch_pt = tuple()
                    self.had_catch = 'yes'
            CheckButton.grid_dict[CheckButton.turn].remove(CheckButton.ckd_pt)
            CheckButton.grid_dict[CheckButton.turn].append(CheckButton.overd_pt)
            CheckButton.grid_dict[2] = list(set([(i, j) for i in range(10) for j in range(10)]) - set(CheckButton.white_grid + CheckButton.black_grid))
            #클릭했던 버튼을 비우는 작업
            unclick_button = CheckButton.button_list[CheckButton.ckd_pt[0]][CheckButton.ckd_pt[1]]
            unclick_button.wbb = 2
            unclick_button.ckd = False
            unclick_button.overd = False
            unclick_button['image'] = CheckButton.image_dict[2]['bg'][(CheckButton.ckd_pt[0]+CheckButton.ckd_pt[1])%2]
            #지금 클릭한 버튼을 채우는 작업, king일 때
            self.wbb = CheckButton.turn
            self.ckd = False
            self.overd = False
            self['image'] = CheckButton.image_dict[CheckButton.turn]['bg'][self.get_bg_num()]
            #전체 자료 정리
            CheckButton.ckd = False
            CheckButton.ckd_pt = (-1, -1)
            CheckButton.overd = False
            CheckButton.overd_pt = (-1, -1)
            if self.had_catch and self.is_continue():
                self.ckd = True
                CheckButton.ckd = True
                CheckButton.ckd_pt = self.get_row_col()
                CheckButton.catch_again = 'yes'
                self['image'] = CheckButton.image_dict[self.wbb]['ckd']
                # 히스토리 저장. 계속 잡고 있는 상황
            else:
                self.had_catch = ''
                CheckButton.catch_again = ''
                CheckButton.turn = 0 if CheckButton.turn == 1 else 1
                # 히스토리 저장. 턴을 넘긴 상황
            self_grid = self.get_row_col()
            if self.wbb == 0 and self_grid[0] ==0:
                self['image'] = CheckButton.image_dict[self.wbb]['king']
                self.is_king = 'king'
                CheckButton.grid_dict[3].append(self.get_row_col())
                print(CheckButton.grid_dict)
            elif self.wbb == 1 and self_grid[0] ==9:
                self['image'] = CheckButton.image_dict[self.wbb]['king']
                self.is_king = 'king'
                CheckButton.grid_dict[4].append(self.get_row_col())
                print(CheckButton.grid_dict)
            h = History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, CheckButton.ckd, CheckButton.ckd_pt,CheckButton.catch_again, CheckButton.king_ckd, CheckButton.history[-1])
            print(h.changed_atts)
            CheckButton.history.append(h)     # History의 인스턴스 속성 중에 changed_pt가 작동을 안함.
            return
        if not CheckButton.catch_again:      # 다시 잡을 수 있는 상황에서 ckd가 False 되지 않도록 하기 위해서 쓴 코드임.
            if CheckButton.ckd and not self.ckd:
                return
            elif CheckButton.ckd and self.ckd:
                self.unckeck_ckd()
            elif not CheckButton.ckd:
                self.check_ckd()
    
    def king_click(self):
        if self.overd:
            gap = abs(CheckButton.ckd_pt[0] - CheckButton.overd_pt[0])
            p_or_m_row = 1 if CheckButton.ckd_pt[0] - CheckButton.overd_pt[0]<0 else -1
            p_or_m_col = 1 if CheckButton.ckd_pt[1] - CheckButton.overd_pt[1]<0 else -1
            opp = 1 if CheckButton.turn ==0 else 0
            opp_king = 4 if CheckButton.turn ==0 else 3
            for i in range(1,gap):
                r = CheckButton.ckd_pt[0] + (p_or_m_row*i)
                c = CheckButton.ckd_pt[1] + (p_or_m_col*i)
                if (r,c) in CheckButton.grid_dict[opp]:
                    # 잡힌 말 속성 정리
                    CheckButton.button_list[r][c].wbb =2
                    # 잡힌 말 이미지 정리
                    CheckButton.button_list[r][c]['image'] = CheckButton.image_dict[2]['bg'][1]
                    # grid_dict 정리
                    CheckButton.grid_dict[opp].remove((r,c))
                    if (r,c) in CheckButton.grid_dict[opp_king]:
                        king_opp_turn = 4 if CheckButton.turn ==0 else 3
                        CheckButton.button_list[r][c].is_king = ''
                        CheckButton.grid_dict[king_opp_turn].remove((r,c))
                        print(CheckButton.grid_dict)
                    self.had_catch = 'yes'
        # grid_dict 정리
            CheckButton.grid_dict[CheckButton.turn].remove(CheckButton.ckd_pt)
            CheckButton.grid_dict[CheckButton.turn].append(CheckButton.overd_pt)
            turn = 3 if CheckButton.turn ==0 else 4
            CheckButton.grid_dict[turn].remove(CheckButton.ckd_pt)
            CheckButton.grid_dict[turn].append(CheckButton.overd_pt)
            CheckButton.grid_dict[2] = list(set([(i, j) for i in range(10) for j in range(10)]) - set(CheckButton.white_grid + CheckButton.black_grid))
            # 클릭했던 버튼을 비우는 작업
            unclick_button = CheckButton.button_list[CheckButton.ckd_pt[0]][CheckButton.ckd_pt[1]]
            unclick_button.wbb = 2
            unclick_button.ckd = False
            unclick_button.overd = False # ?
            unclick_button.is_king = False
            unclick_button['image'] = CheckButton.image_dict[2]['bg'][(CheckButton.ckd_pt[0]+CheckButton.ckd_pt[1])%2]
            # 지금 클릭한 버튼을 채우는 작업
            self.is_king = True
            self.wbb = CheckButton.turn
            self.ckd = False
            self.overd = False
            self['image'] = CheckButton.image_dict[CheckButton.turn]['king']
            # 전체 자료 정리
            CheckButton.ckd = False
            CheckButton.ckd_pt = (-1, -1)
            CheckButton.overd = False
            CheckButton.overd_pt = (-1, -1)
            CheckButton.king_ckd = ''
            if self.had_catch and self.is_king_continue():
                self.ckd = True
                CheckButton.ckd = True
                CheckButton.ckd_pt = self.get_row_col()
                CheckButton.catch_again = 'yes'
                CheckButton.king_ckd = 'yes'
                self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
                # 히스토리 저장. 계속 잡고 있는 상황
            else:
                self.had_catch = ''
                CheckButton.catch_again = ''
                CheckButton.turn = 0 if CheckButton.turn == 1 else 1
            h = History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, CheckButton.ckd, CheckButton.ckd_pt,CheckButton.catch_again, CheckButton.king_ckd, CheckButton.history[-1])
            print(h.changed_atts)
            CheckButton.history.append(h)
            return
        # if not CheckButton.catch_again:      # 다시 잡을 수 있는 상황에서 ckd가 False 되지 않도록 하기 위해서 쓴 코드임.
        #     if CheckButton.ckd and not self.ckd:
        #         return
        #     elif CheckButton.ckd and self.ckd:
        #         print('ok')
        #         self.unckeck_ckd()
        #     elif not CheckButton.ckd:
        #         print('b')
        #         self.check_ckd()
    
    def over(self,event):
        if CheckButton.ckd==False:
            return
        if CheckButton.overd:
            return
        if self.wbb==0 or self.wbb==1:
            return
        move_or_catch = self.is_over_ok()
        if not move_or_catch:
            return
        CheckButton.overd_pt = self.get_row_col()
        if CheckButton.king_ckd:
            gap = abs(CheckButton.ckd_pt[0] - CheckButton.overd_pt[0])
            p_or_m_row = 1 if CheckButton.ckd_pt[0] - CheckButton.overd_pt[0]<0 else -1
            p_or_m_col = 1 if CheckButton.ckd_pt[1] - CheckButton.overd_pt[1]<0 else -1
            for i in range(1,gap):
                r = CheckButton.ckd_pt[0] + (p_or_m_row*i)
                c = CheckButton.ckd_pt[1] + (p_or_m_col*i)
                if (r,c) in CheckButton.grid_dict[CheckButton.turn]:
                    CheckButton.overd_pt = (-1,-1)
                    # 데이터 전송
                    return
        self.overd = True
        CheckButton.overd = True
        self['image']=CheckButton.image_dict[CheckButton.turn]['mover']
        if move_or_catch == 'catch':
            CheckButton.catch_pt = int((self.get_row_col()[0]+CheckButton.ckd_pt[0])/2), int((self.get_row_col()[1]+CheckButton.ckd_pt[1])/2)
            # 데이터 전송
            # print(CheckButton.catch_pt)
            #잡으려면 이다음에 바로 클릭을 할 것이다. 그럼 클릭 메소드에서 잡는 행동을 실행해줘야한다.
        # if CheckButton.catch_pt and (CheckButton.ckd_pt[0]+CheckButton.overd_pt[0])/2 == CheckButton.catch_pt[0] and (CheckButton.ckd_pt[1]+CheckButton.overd_pt[1])/2 == CheckButton.catch_pt[1]:
        #     self['image']=CheckButton.image_dict[CheckButton.turn]['mover']

    def is_continue(self):
        opp = 0 if CheckButton.turn == 1 else 1
        opp_grid = CheckButton.grid_dict[opp]
        self_grid = self.get_row_col()
        right_up = (self_grid[0]-1,self_grid[1]+1)
        right_down = (self_grid[0]+1,self_grid[1]+1)
        left_up = (self_grid[0]-1,self_grid[1]-1)
        left_down = (self_grid[0]+1,self_grid[1]-1)
        is_opp_continue = right_up in opp_grid or right_down in opp_grid\
             or left_up in opp_grid or left_down in opp_grid
        if is_opp_continue:
            # 잡을 수 있는 말이 있으면 ckd 상태 유지
            right_up_blank = (right_up[0]-1,right_up[1]+1)
            right_down_blank = (right_down[0]+1,right_down[1]+1)
            left_up_blank = (left_up[0]-1,left_up[1]-1)
            left_down_blank = (left_down[0]+1,left_down[1]-1)
            catch_again = (right_up in opp_grid and right_up_blank in CheckButton.grid_dict[2])\
                 or (right_down in opp_grid and right_down_blank in CheckButton.grid_dict[2])\
                      or (left_up in opp_grid and left_up_blank in CheckButton.grid_dict[2])\
                           or (left_down in opp_grid and left_down_blank in CheckButton.grid_dict[2]) 
        else:
            return False
        return catch_again

    def is_king_continue(self):
        r = self.get_row_col()[0]
        c = self.get_row_col()[1]
        left_up_cnt = r if r<=c else c
        right_up_cnt = r if r<=9-c else 9-c
        left_down_cnt = 9-r if 9-r<=c else c
        right_down_cnt = 9-r if 9-r<=9-c else 9-c
        cnt = [left_up_cnt, right_up_cnt, left_down_cnt, right_down_cnt]
        opp = 1 if CheckButton.turn ==0 else 0
        # left_up
        for i in range(1, left_up_cnt+1):
            opp_button = (r-i,c-i)
            blank = (r-i-1,c-i-1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # right_up
        for i in range(1, right_up_cnt+1):
            opp_button = (r-i,c+i)
            blank = (r-i-1,c+i+1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # left_down
        for i in range(1, left_down_cnt+1):
            opp_button = (r+i,c-i)
            blank = (r+i+1,c-i-1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # right_down
        for i in range(1, right_down_cnt+1):
            opp_button = (r+i,c+i)
            blank = (r+i+1,c+i+1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue

    # 이동하려는 칸이 조건에 맞는지 확인하는 메소드 >> 클릭드포인트 위 대각 1칸이면 True 이게 아니면 False
    def is_over_ok(self):
        opp = 0 if CheckButton.turn == 1 else 1
        is_two = self.get_row_col() == (CheckButton.ckd_pt[0]-2, CheckButton.ckd_pt[1]-2)\
             or self.get_row_col() == (CheckButton.ckd_pt[0]-2, CheckButton.ckd_pt[1]+2)
        is_two_back = self.get_row_col() == (CheckButton.ckd_pt[0]+2, CheckButton.ckd_pt[1]-2)\
             or self.get_row_col() == (CheckButton.ckd_pt[0]+2, CheckButton.ckd_pt[1]+2)
        midde_pt = int((self.get_row_col()[0]+CheckButton.ckd_pt[0])/2), int((self.get_row_col()[1]+CheckButton.ckd_pt[1])/2)
        is_opp = (is_two or is_two_back) and midde_pt in CheckButton.grid_dict[opp]
        p_or_m = -1 if CheckButton.turn ==0 else 1
        is_move = self.get_row_col() == (CheckButton.ckd_pt[0]+p_or_m, CheckButton.ckd_pt[1]-1)\
                 or self.get_row_col() == (CheckButton.ckd_pt[0]+p_or_m, CheckButton.ckd_pt[1]+1)
        king_is_move = abs(self.get_row_col()[0] - CheckButton.ckd_pt[0]) == abs(self.get_row_col()[1] - CheckButton.ckd_pt[1])


        if CheckButton.king_ckd and king_is_move:
            # 여기 조건 다시 코드 짜기
            return 'king_move' 
        if is_move:
            return 'move'
        if is_opp:
            return 'catch'
        return ''

    def out(self,event):
        if CheckButton.ckd==False:
            return
        if not CheckButton.overd:
            return
        if self.wbb==0 or self.wbb==1:
            return
        self.overd = False
        CheckButton.overd = False
        CheckButton.overd_pt = (-1, -1)
        self['image']=CheckButton.image_dict[self.wbb]['bg'][self.get_bg_num()]
        CheckButton.catch_pt = tuple()
        # 데이터 전송

    def unckeck_ckd(self): # king일 때 메소드
        self.change_ckd(False)
        CheckButton.ckd_pt = (-1, -1)
        if self.is_king:
            print('a')
            self['image'] = CheckButton.image_dict[self.wbb]['king']
            CheckButton.king_ckd = ''
        else:
            self['image'] = CheckButton.image_dict[self.wbb]['bg'][self.get_bg_num()]

    def check_ckd(self):
        self.change_ckd(True)
        CheckButton.ckd_pt = self.get_row_col()
        if self.is_king:
            self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
            CheckButton.king_ckd = 'yes'
        else:
            self['image'] = CheckButton.image_dict[self.wbb]['ckd']

    def get_row_col(self):
        grid_info = self.grid_info()
        row, col = grid_info['row'], grid_info['column']
        return row, col

    def get_bg_num(self):
        row, col = self.get_row_col()
        return (row+col)%2
    
    def change_ckd(self, ckd):
        self.ckd = ckd
        CheckButton.ckd = any([bt.ckd for row_bt in CheckButton.button_list for bt in row_bt])

    @staticmethod
    def undo(event):
        
        if len(CheckButton.history) ==1:
            print("cannot ctrl + z")
        else:
            # 클래스 속성 되돌리기
            CheckButton.grid_dict = copy.deepcopy(CheckButton.history[-2].grid)
            CheckButton.turn = CheckButton.history[-2].turn
            CheckButton.ckd = CheckButton.history[-2].ckd
            CheckButton.ckd_pt = CheckButton.history[-2].ckd_pt
            CheckButton.catch_again = CheckButton.history[-2].catch_again
            CheckButton.king_ckd = CheckButton.history[-2].king_ckd
            # 버튼 이미지 되돌리기
            CheckButton.button_list = []
            CheckButton.make_button()
            # king 이미지 씌우기
            # if CheckButton.grid_dict[3]:
            #     for button in CheckButton.grid_dict[3]:
            #         CheckButton.button_list[button[0]][button[1]]['image'] = CheckButton.image_dict[0]['king']
            #         CheckButton.button_list[button[0]][button[1]].is_king = 'yes'
            # if CheckButton.grid_dict[4]:
            #     for button in CheckButton.grid_dict[4]:
            #         CheckButton.button_list[button[0]][button[1]]['image'] = CheckButton.image_dict[1]['king']
            #         CheckButton.button_list[button[0]][button[1]].is_king = 'yes'
            # CheckButton의 instance의 is_king 속성 'yes'로 만들기
            if CheckButton.grid_dict[3]:
                for button in CheckButton.grid_dict[3]:
                    CheckButton.button_list[button[0]][button[1]].is_king = 'yes'
            if CheckButton.grid_dict[4]:
                for button in CheckButton.grid_dict[4]:
                    CheckButton.button_list[button[0]][button[1]].is_king = 'yes'
            # 되돌린 상태에서 catch_again이 'yes'일 때
            if CheckButton.catch_again:
                for i in CheckButton.grid_dict[CheckButton.turn]:
                    if i == CheckButton.ckd_pt:
                        CheckButton.button_list[i[0]][i[1]].ckd = True
                        CheckButton.button_list[i[0]][i[1]].had_catch = 'yes'
                        if CheckButton.king_ckd:
                            CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['king_ckd']
                        else:
                            CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['ckd']
            # Control + z한 상태를 다시 history에 넣기
            # h = History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, CheckButton.ckd, CheckButton.ckd_pt,CheckButton.catch_again, CheckButton.history[-1])
            # CheckButton.history.append(h)

            CheckButton.history.remove(CheckButton.history[-1])
        return


    @classmethod
    def make_grid_dict(cls):
        for i in range(4):
            for j in range(10):
                if i%2==0 and j%2!=0:
                    CheckButton.black_grid.append((i,j))
                elif i%2!=0 and j%2==0:
                    CheckButton.black_grid.append((i,j))

#함수 형태로 짜는 것(블럭지어서)
        for i in range(6,10):
            for j in range(10):
                if i%2==0 and j%2!=0:
                    CheckButton.white_grid.append((i,j))
                elif i%2!=0 and j%2==0:
                    CheckButton.white_grid.append((i,j))

        cls.grid_dict = {
            0 : CheckButton.white_grid,
            1 : CheckButton.black_grid,
            2 : list(set([(i, j) for i in range(10) for j in range(10)]) - set(CheckButton.white_grid +CheckButton.black_grid)),
            3:[],
            4:[]
        }

        cls.history = [History(copy.deepcopy(cls.grid_dict), 0, False, (-1,-1),'','')]  # 왜 class에 안 들어가지? 가능함.

    @classmethod
    def make_button(cls):
        item = list(CheckButton.grid_dict.items())[0:3]
        for i in range(10):
            cls.button_list.append([])
            for j in range(10):
                position = (i, j)
                for wbb, pts in item:
                    if position in pts:
                        #책버튼으로 인스턴스를 생성했다. 그 위치는 [i][j]에 있다.
                        if position in CheckButton.grid_dict[3]:
                            cls.button_list[i].append(cls(root, wbb=0, image=cls.image_dict[0]['king']))  
                        elif position in CheckButton.grid_dict[4]:
                            cls.button_list[i].append(cls(root, wbb=1, image=cls.image_dict[1]['king']))
                        else:
                            cls.button_list[i].append(cls(root, wbb=wbb, image=cls.image_dict[wbb]['bg'][(i+j)%2]))
                        cls.button_list[i][j].grid(row=i, column=j)
                        cls.button_list[i][j]['command'] = cls.button_list[i][j].click
                        break
    
    @classmethod
    def get_values(cls):
        data_list = get_data_list(cls.var_names)
        return get_json(cls.var_names, data_list)
    
    @classmethod
    def put_values(cls, json_values):
        # json 풀기, grid_dict 의 키를 int로 변환
        all_values = json.loads(json_values)
        for i in range(0,5):
            all_values['grid_dict'][i] = all_values['grid_dict'].pop(str(i))
        print(all_values)
        # 전체 데이터 적용
        CheckButton.grid_dict = copy.deepcopy(all_values['grid_dict'])
        CheckButton.turn = all_values['turn']
        CheckButton.ckd = all_values['ckd']
        CheckButton.ckd_pt = copy.copy(all_values['ckd_pt'])
        CheckButton.overd = all_values['overd']
        CheckButton.overd_pt = copy.copy(all_values['overd_pt'])
        CheckButton.catch_pt = copy.copy(all_values['catch_pt'])
        CheckButton.catch_again = all_values['catch_again']
        CheckButton.king_ckd = all_values['king_ckd']
        # 각 버튼에 적용, make_button 버튼들의 기본 이미지, wbb 세팅, 체크된 버튼 적용, 오버된 버튼 적용, 클릭된 버튼, 오버된 버튼
        # for i in range(10):
        #     for j in range(10):

        pass

    

# 그리드 딕트 클래스 변수로 넣기

# def click(pt, color):
#     button_list[pt[0]][pt[1]]['image'] = CheckButton.image_dict[color]['ckd']

# 클래스 메소드(버튼 리스트 채우기)

CheckButton.image_dict={
    0:{
    'bg' : [PhotoImage(file=path + "/checker_folder/white_bg0.png"), PhotoImage(file=path + "/checker_folder/white_bg1.png")],
    'ckd' : PhotoImage(file=path + "/checker_folder/white_ckd.png"),
    'mover' : PhotoImage(file=path + "/checker_folder/white_mover.png"),
    'king' : PhotoImage(file=path + "/checker_folder/white_king.png"),
    'king_ckd' : PhotoImage(file=path + "/checker_folder/white_king_ckd.png")
    },
    1:{
    'bg' : [PhotoImage(file=path + "/checker_folder/black_bg0.png"), PhotoImage(file=path + "/checker_folder/black_bg1.png")],
    'ckd' : PhotoImage(file=path + "/checker_folder/black_ckd.png"),
    'mover' : PhotoImage(file=path + "/checker_folder/black_mover.png"),
    'king' : PhotoImage(file=path + "/checker_folder/black_king.png"),
    'king_ckd' : PhotoImage(file=path + "/checker_folder/black_king_ckd.png")
    },
    2:{
    'bg' : [PhotoImage(file=path + "/checker_folder/blank_bg0.png"), PhotoImage(file=path + "/checker_folder/blank_bg1.png")]
    }}

CheckButton.make_grid_dict()
CheckButton.make_button()
# print(list(CheckButton.grid_dict.items())[0:3])
root.bind("<Control - z>", CheckButton.undo)
# print(CheckButton.history[-1].turn)

json_values = CheckButton.get_values()
print(CheckButton.put_values(json_values))
if __name__=='__main__':  # 주가 될 때만 실행되도록(원래 모듈 불러올 때 끝까지 다 해석해서 실행한다.)
    root.mainloop()

# history_thread.join()

# 왜  mainloop 안에다가 쓰레드를 만들지?