import threading,time
from threading import Lock

lock = Lock()
account =0
who_dict = {'t1' : {'money': 0}, 't2' : {'when':[] , 'money': 0}, 't3' : {'money': 0}}
amount_history = []

def put_to_acount(name):
    global account, who_dict, amount_history
    time.sleep(0.05)
    while account < 200000:  # != 으로 하면 동시에 두 개 이상의 thread가 거의 동시에 조건을 만족시키고 수행문으로 들어왔을 때 21000이 되고 그러면 무한 반복이 된다. 그래서 조건은 범위로 만드는 것이 좋다.
        # lock.acquire()   # Lock 알아보기
        account += 10000
        amount_history.append(name)
        who_dict[name]['money'] += 10000
        # lock.release()
        time.sleep(0.05)
    return

t1 = threading.Thread(target=put_to_acount, args=('t1', ))
t2 = threading.Thread(target=put_to_acount, args=('t2', ))
t3 = threading.Thread(target=put_to_acount, args=('t3', ))

t1.start()
t2.start()
t3.start()

t1.join()
t2.join()
t3.join()

print(who_dict)
print(amount_history)
