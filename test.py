# x=float(input())

# def f(n):
#     if n<0 and n!=int(n):
#         return int(n)
#     elif n>0 and n!=int(n):
#         return int(n)+1
#     else:
#         return int(n)

# print(f(x))





from checker_k import CheckButton


class Cal():
    def __init__(self,first,second):
        self.first=first
        self.second=second
        self.result = self.add()
    def add(self):
        result=self.first+self.second
        return result
    def sub(self):
        result=self.first-self.second
        return result
    def mul(self):
        result=self.first*self.second
        return result
    def div(self):
        result=self.first/self.second
        return result

a=Cal(4,2)
print(a.result)
# print(a.add())
# print(a.sub())
# print(a.mul())
# print(a.div())

class MoreCal(Cal):
    def pow(self):
        result=self.first**self.second
        return result

# b=MoreCal(4,2)
# print(b.pow())

class SaveCal(MoreCal):
    def div(self):
        if self.second==0:
            return 0
        else:
            result=self.first/self.second
            return result

# c=SaveCal(2,0)
# print(c.div())

class Person:
    bag=[]

    def __init__(self,*c):
        self.name=c[0]
        self.age=c[1]
        self.__wallet=c[2]
    
    def __greeting(self):
        print('hello')
    
    def hello(self):
        self.__greeting()

# psn=Person(*['name',22])
# print(psn.name)
# print(psn.age)

# psn1=Person('name',22)

# psn1.hello()
# psn1.__greeting()

class PSN:
    __slots__=['name','age']

# maria=PSN()
# maria.name='maria'
# maria.age=22
# print(maria.name)
# print(maria.age)
# maria.address='USA'
# print(maria.address)

# psn2=Person('name',22,1000000000000)
# print(psn2.__wallet)

# print(Person.bag)

# Person.bag=['책']

# print(Person.bag)

# psn3=Person('name',22,'1000000000000')
# print(psn3.name)
# psn3.name='name1'
# print(psn3.name)

# from tkinter import*
# root=Tk()

# img=PhotoImage(root,file="ck/black_bg0.png")
# label=Label(root,image=img)
# label.pack()

# root.mainloop()

# a='15.0'
# print(float(a))

# from tkinter import*

# root=Tk()

# def click():
#     global btn
#     count=0
#     for i in range(5):
#         if count%2==0:
#             btn['bg']='black'
#             count+=1
#         else:
#             btn['bg']='white'
#             count+=1


# btn=Button(width=100,height=100,command=click)
# # btn.bind("<Button-1>",click)
# btn.grid(row=0,column=0)

# root.mainloop()




# t = [1,2,3,4,5]
# t1 = t+[1]
# print(t1)

# a = 2-3-4
# print(a)

# b = 15/5/2
# print(b)

# c = d =1
# c=2        #같이는 못 바꾼다.
# print(id(c))
# print(id(d))
# print(c)
# print(d)

# x = 1
# y = 1
# if x or y:
#     print('yes')
# else:
#     print('no')

# a = 3.14
# print(int(a))

# b = float(input())
# print(b)

# import copy

# dict = {0:'a', 1:'b'}
# dict_1 = dict.copy()
# print(dict_1)
# dict[0] = 'c'
# print(dict)
# print(dict_1)


# n = 'a'
# m = n.copy()

# print(n is m)
# print(id(n), id(m))

# k = 'a'
# print(id(k))
# n = 'b'
# print(id(n), id(m))

# 사용자로부터 입력받은 수를 계속 누적해 합계를 출력하는 프로그램

# 1)사용자가 수를 몇번이나 입력할지 모르므로, 조건을 True 무한반복
# 2)예외상항. 그만을 입력할 경우, break문으로 반복 중지
# 3)예외상황. 입력한 텍스트가 수가 아닐 경우, 이 경우에는 덧셈을 할 수 
# 없으므로 수를 더하는 명령을 실행하면 안되서, 잘못된 입력입니다.를 출력해서 continue 문으로 다음 주기로 넘어간다. 


# sum_num = 0
# while True:
#     num = input()
#     if num == '그만':
#         break
#     if not num.isnumeric():
#         print('잘못된 입력입니다.')
#         continue
#     sum_num += int(num)
#     print(sum_num)

# a = list(range(5))   # a = [range(5)]를 출력하면 [range(0, 5)]라고 출력된다.
# print(a)

# t = [True, False]
# print(t)

# t = (True, False)
# print(t)

# t = (tuple(), tuple())

# # t[0] = (1,0)
# # print(t)

# tmp_list = list(range(10,-1,-1))
# print(tmp_list)

# s = sum((0,11))
# print(s)

def king_click(self):
    if self.overed:
        # grid_dict 정리
        CheckButton.grid_dict[CheckButton.turn].remove(CheckButton.ckd_pt)
        CheckButton.grid_dict[CheckButton.turn].append(CheckButton.overd_pt) 
        turn = 3 if CheckButton.turn ==0 else 4
        CheckButton.grid_dict[turn].remove(CheckButton.ckd_pt)
        CheckButton.grid_dict[turn].append(CheckButton.overd_pt)
        CheckButton.grid_dict[2] = list(set([(i, j) for i in range(10) for j in range(10)]) - set(CheckButton.white_grid + CheckButton.black_grid))
        # 클릭했던 버튼을 비우는 작업
        unclick_button = CheckButton.button_list[CheckButton.ckd_pt[0]][CheckButton.ckd_pt[1]]
        unclick_button.wbb = 2
        unclick_button.ckd = False
        unclick_button.overd = False # ?
        unclick_button.is_king = False
        unclick_button['image'] = CheckButton.image_dict[2]['bg'][(CheckButton.ckd_pt[0]+CheckButton.ckd_pt[1])%2]
        # 지금 클릭한 버튼을 채우는 작업
        self.is_king = True
        self.wbb = CheckButton.turn
        self.ckd = False
        self.overd = False
        self['image'] = CheckButton.image_dict[CheckButton.turn]['king']
        # 전체 자료 정리
        CheckButton.ckd = False
        CheckButton.ckd_pt = (-1, -1)
        CheckButton.overd = False
        CheckButton.overd_pt = (-1, -1)
        CheckButton.king_ckd = ''
        if not CheckButton.catch_again:      # 다시 잡을 수 있는 상황에서 ckd가 False 되지 않도록 하기 위해서 쓴 코드임.
            if CheckButton.ckd and not self.ckd:
                return
            elif CheckButton.ckd and self.ckd:
                self.unckeck_ckd()
            elif not CheckButton.ckd:
                self.check_ckd()

dict = {'0' : [1,2,3,4,5]}

print(dict)

dict[0] = dict.pop('0')

print(dict)