import socket, threading, time
# from checker_k import CheckButton

ipv4 = '127.0.0.1'
port = 9999 #0 ~ 65573

client_socket = socket.socket(family = socket.AF_INET, type = socket.SOCK_STREAM)

# client_socket.bind((ipv4, port))

client_socket.connect((ipv4, port))


def send_msg():
    while True:
        send_str = input()
        client_socket.send(send_str.encode())
        time.sleep(0.05)

def recv_msg():
    while True:
        recv_str = client_socket.recv(4096).decode()
        print(recv_str)


send_th = threading.Thread(target=send_msg)
recv_th = threading.Thread(target=recv_msg)
recv_th.daemon = True

send_th.start()
recv_th.start()



# while True:
#     send_data = CheckButton.get_values()
#     input()
#     client_socket.send(send_data.encode()) #byte로 바꿔서 전송해야함.
    # recv_data = client_socket.recv(1024).decode()
    # print(recv_data)