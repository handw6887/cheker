import threading, time

def sum_part(s,e,classify = ''):
    result =0
    for i in range(s,e+1):
        time.sleep(0.5)
        result += i
        print(classify + str(result))

n =0

def sum_input(classify_mark):
    global n
    while True:
        a,b = map(int, input().split())
        th_str = classify_mark+str(n)+ ' : '
        n +=1
        sub_th = threading.Thread(target=sum_part, args=(a, b, th_str))
        sub_th.daemon = True
        sub_th.start()


t1 = threading.Thread(target=sum_input, args=('from_t',))
t1.start()
