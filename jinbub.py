import time

#변환할 10진수를 입력 받는다.
ten=int(input("변환할 십진수 입력"))


n=int(input("몇진법으로?"))

#숫자로 쓸 기호 세트를 정한다.0~9(10개),A~Z(26개)2~36진법
#n<=10, n>10

giho=''
if n<=10:
    giho=''.join([str(i) for i in range(n)])

if 10<n<37:
    giho=''.join([str(i) for i in range(10)])
    giho+=''.join([chr(j) for j in range(ord('A'),ord(chr(ord('A')+(n-10))))])


count=0
num=[]
if ten<n:
    for i in range(ten+1):
        num.append(giho[i])
    print(num[-1])
else:
    for i in range(n):
        num.append(giho[i])
    for i in range(1,n):
        for j in range(n):
            num.append(giho[i]+giho[j])
    count=1
    a=n
    while True:
        if len(num[-1])==count+1:
            for i in range(a,len(num)):
                for j in range(n):
                    num.append(str(num[i]+giho[j]))
            count+=1
            a*=n
        if len(num)>ten:
            break
    print(num[ten])


#진행 과정에서 변하는 부분과 변하지 않는 부분을 구분하고 변하지 않는 부분은 미리 고정된 코드로 결정해두고 변하는 부분을 반복문으로 코드르 짤 때 변하지 않는 부분의 코드를 이용할 수 있다.
#큰 진행 과정 전체를 생각하면 코드를 짜기 힘들 수도 있다. 그러므로 큰 진행 과정에 필요한 작은 과정들로 나누어서 생각해보기
#코드의 결과의 규칙성이 깨지는 경계를 떠올리기. 예를 들면 십진법까지는 0~9까지롤 표현가능하다. 하지만 십진법을 넘어가면 알파벳이 필요하다. 이때 십진법이라는 경계를 인식하기. 그리고 언제 자리 수를 올려할지 생각하기. 이런 식으로 작은 과정으로 나눠서 순차적으로 문제를 해결해나가기.