import threading, uuid, time

sum = 0

def print_num(s_num, e_num):
    global sum
    func_id = str(uuid.uuid4())[:7]  #고유한 id 만들 때 uuid를 쓴다.
    for i in range(s_num,e_num+1):
        sum+=i
        print(i, ' >> ', func_id)
        time.sleep(0.05)

t1 = threading.Thread(target=print_num, args=(1,100000))
t2 = threading.Thread(target=print_num, args=(1,100000))

t1.start()
# t1.join()  #t1 먼저 실행하고 다 끝나면 t2를 실행한다.
t2.start()
# t2.join() #안하면 print(sum)이 먼저 출력될 수도 있다. 스케줄링 할 때 블락을 랜덤하게 걸기 때문이다.

t1.join()
t2.join()

print(sum)