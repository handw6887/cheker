# import sys

# for i in sys.stdin:
#     print(i)


# print('a');print('b')

# y=3.14
# print('%0.3f' %y)

#람다식: return이 한 줄에 끝날 수 있을 때/일회적인 함수를 만들 때 주로 사용한다.

# def add(a,b):
#     return a+b

# add_func = lambda a,b : a+b

# add_many_func = lambda *c:sum(list(c))

# re=add_many_func(1,2,3,4,5)

# result=add_func(1,2)
# print(result)










#map 함수

ex_list = [str(i+1) for i in range(10)]

#숫자로 변환한 다음에 10을 더하고 싶다.
# for i in ex_list:
#     ex_list.append(i+10)   #변환을 두번 해야됨

result_list=list(map(int,ex_list))   #list() 해줘야 리스트로 돌려줌.

result = list(map(lambda i:int(i)+10,ex_list))


#filter 함수
#reduce 함수 두개 다 map함수랑 비슷하게 쓰임. reduce는 선호도가 낮음.


files=['font','1.png','10.jpg','11.gif','2.jpg','3.png','table.xslx','spec.dox']

def finder(l):
    for i in l:
        if '.jpg' in i or '.png' in i:
            return i

#여러 가지로 표현해보기(람다만, 람다+맵 or 필터)

def f(i):
    return '.jpg' in i or '.png' in i

print(list(filter(lambda i : '.jpg' in i or '.png' in i,files)))
