import socket, json, threading

ipv4 = '127.0.0.1' # local host 주소 지금 자기 자신의 컴퓨터의 주소
port = 9999 #0 ~ 65573

server_socket = socket.socket(family = socket.AF_INET, type = socket.SOCK_STREAM)

server_socket.bind((ipv4, port))

server_socket.listen() # server라는 증거이다. # 듣는 모드로 있겠다는 의미이다.

socket_list = []

def loop_comunication(soc, addr): # 서버 측에서 갖고 있는 soc(각각의 클라이언트와 연결)
    while True:
        try:
            recv_msg = soc.recv(4096).decode()
        except:
            print(addr[1], "disconnected!")
            socket_list.remove((soc,addr))
            break
        recv_msg = str(addr[1]) + ' : ' + recv_msg
        print(recv_msg)
        for i in socket_list:
            if addr[1] == i[1][1]:
                continue
            i[0].send(recv_msg.encode())

def send_msg():
    while True:
        send_str = input()
        send_str = 'server_msg' + ' : ' + send_str
        for i in socket_list:
            i[0].send(send_str.encode())

main_th = threading.Thread(target=send_msg)
main_th.daemon = False
main_th.start()


th_list = []

def client_connection():
    while True:
        socket_list.append(server_socket.accept()) # 접속이 되었을 때 작동
        print(socket_list[-1][1])
        th_list.append(threading.Thread(target=loop_comunication, args=socket_list[-1]))
        th_list[-1].daemon = True
        th_list[-1].start()


cl_con = threading.Thread(target=client_connection)
cl_con.daemon = True
cl_con.start()

# while True:
#     recv_data = client_socket.recv(4096).decode() # 받을 때는 디코드하기
#     recv_data = json.loads(recv_data)
#     print(recv_data)
#     # client_socket.send(recv_data.encode())
    
