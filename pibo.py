#피보나치 수열 1,1,2,3,5,8,13,21,.....
#피보나치 수열 문제 재귀적으로 구현할때 지수적으로 매우 많이 호출되어서
#효율이 떨어지는 문제를 어떻게 해결할까?
#재귀 함수 예제도 검색해서 많이 공부해 보기

# def pibo(n):                       #재귀함수
#     if n==1 or n==2:
#         return 1
#     return pibo(n-2) + pibo(n-1)   #수가 조금만 커져도 호출 수가 증가한다. 시간이 많이 든다. 지수를 이용

# count=0

# def pibo_cnt(n):
#     global count
#     if n==1 or n==2:
#         count+=1
#         print(count)
#         return 1
#     return pibo(n-2) + pibo(n-1)

#반복문을 통한 피보나치 수열 함수
# def pibo(n):
#     num=[1,1]
#     for i in range(n-2):
#         num=num+[(num[i]+num[i+1])]
#     return num[-1]
# print(pibo(100))

num=[1,1]
def pibo(n):              #n은 1이상의 자연수
    global num
    if len(num)>=n:
        return num[n-1]
    else:
        num.append(pibo(n-2) + pibo(n-1))
        return num[n-1]

print(pibo(1995))

    