import threading, random, time, copy
from threading import Lock

lock = Lock()

num_list = [i for i in range(10)]
copied_num_list = num_list[:]


def change_state():
    global num_list
    while True:
        rnd_time = random.uniform(2,5)
        which_index = random.randrange(10)
        time.sleep(rnd_time)
        lock.acquire()
        num_list[which_index] = random.randrange(0, 9)
        lock.release()
        #print('원본 변경')

def detect_change():
    global num_list, copied_num_list
    while True:
        if num_list != copied_num_list:
            print('변경 전 : ', copied_num_list, '\n변경 후 : ', num_list)
            copied_num_list = copy.copy(num_list)


t1 = threading.Thread(target=change_state)
t2 = threading.Thread(target=detect_change)
# t2.daemon = True

t1.start()
t2.start()

t1.join()
t2.join()