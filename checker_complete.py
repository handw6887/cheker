from tkinter import Canvas, Tk,Button,PhotoImage
import os, threading, copy, time, json
from ck_history import History
from checker_client import client_socket

def recv_checker():
    while True:
        recv_str = client_socket.recv(4096).decode()
        # print(recv_str)
        recv_dict = json.loads(recv_str)
        if 'my_turn' in recv_dict:
            myturn.append(int(recv_dict['my_turn']))
        elif 'cls' in recv_dict:
            ins = CheckButton
            getattr(ins, 'recv_'+recv_dict['method'])(recv_dict['turn'], recv_dict['grid'])
        elif 'history' in recv_dict:
            ins = CheckButton
            getattr(ins, 'recv_'+recv_dict['method'])(recv_dict['history'])
        else:
            ins = CheckButton.button_list[recv_dict['grid'][0]][recv_dict['grid'][1]]
            getattr(ins, 'recv_'+recv_dict['method'])()

threading.Thread(target=recv_checker, daemon=True).start()

path = os.getcwd()

myturn = []

root=Tk()

class CheckButton(Button):
    my_turn = myturn[0]
    image_dict = {}
    button_list = []
    white_grid = []
    black_grid = []
    turn = 0 #누구의 턴인지. 0:화이트, 1:블랙
    ckd = tuple()
    overd = tuple()

    def __init__(self, master=None, wbb=None, ckd=False, cnf={}, **kw):
        super().__init__(master, cnf, **kw)
        self.wbb=wbb #돌의 색깔 0:화이트, 1:블랙, 2:빈칸
        self.ckd=ckd #돌이 클릭되었는지. True:클릭상태, False:클릭안된상태
        self.overd = False
        self.bind("<Enter>",self.over)
        self.bind("<Leave>",self.over_out)
        self.is_king = False
        self.had_catch = False

    def click(self):
        if CheckButton.turn != CheckButton.my_turn:
            return
        if self.is_king:
            self.king_click()
            return
        if CheckButton.turn == self.wbb and not self.had_catch:
            if CheckButton.ckd:
                if self.ckd and not self.is_king:
                    self.uncheck_ckd() # 클릭 이미지 풀기
                else:
                    return
            else:
                self.check_ckd() # 클릭 이미지로 바꾸기
        elif CheckButton.ckd and self.wbb ==2:
            if CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]].is_king:
                self.king_click()
                return
            if self.overd:
                grid = self.get_row_col()
                self.change_grid_dict(CheckButton.ckd, grid)
                self.just_move()
                if (self.wbb ==0 and grid[0] ==0) or (self.wbb == 1 and grid[0] ==9):
                    self.make_king()
            else:
                return
            if self.had_catch and self.can_catch_again():
                self.ckd = True
                CheckButton.ckd = self.get_row_col()
                self['image'] = CheckButton.image_dict[self.wbb]['ckd']
                send_msg = {'method' : 'catch_again', 'grid' : self.get_row_col()}
                client_socket.send(json.dumps(send_msg).encode())
            else:
                self.had_catch = False
                grid = self.get_row_col()
                CheckButton.change_turn(grid)
                CheckButton.history = [History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, tuple())]
            if CheckButton.history[-1].turn == CheckButton.turn:
                h = History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, CheckButton.ckd, CheckButton.history[-1])
                print(h.changed_atts)
                CheckButton.history.append(h)
                print(CheckButton.history)
            else:
                pass
        else:
            return

    def king_click(self):
        if CheckButton.turn == self.wbb and not self.had_catch:
            if CheckButton.ckd:
                if self.ckd:
                    self.king_uncheck_ckd()
                else:
                    return
            else:
                self.king_check_ckd()
        elif CheckButton.ckd and self.wbb ==2:
            if self.overd:
                grid = self.get_row_col()
                self.king_change_grid_dict(CheckButton.ckd, grid)
                self.king_just_move()
            else:
                return
            if self.had_catch and self.king_can_catch_again():
                self.ckd = True
                CheckButton.ckd = self.get_row_col()
                self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
                send_msg = {'method' : 'king_catch_again', 'grid' : self.get_row_col()}
                client_socket.send(json.dumps(send_msg).encode())
            else:
                self.had_catch = False
                grid = self.get_row_col()
                CheckButton.change_turn(grid)
                CheckButton.history = [History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, tuple())]
            if CheckButton.history[-1].turn == CheckButton.turn:
                h = History(copy.deepcopy(CheckButton.grid_dict), CheckButton.turn, CheckButton.ckd, CheckButton.history[-1])
                print(h.changed_atts)
                CheckButton.history.append(h)
            else:
                pass
        else:
            return

    @classmethod
    def change_turn(cls,grid):
        opp = 1 if cls.turn ==0 else 0
        cls.turn = opp
        send_msg = {'method' : 'change_turn', 'turn' : CheckButton.turn, 'cls' : 0, 'grid': grid}
        client_socket.send(json.dumps(send_msg).encode())
        return

    @classmethod
    def recv_change_turn(cls, turn, grid):
        cls.turn = turn
        cls.button_list[grid[0]][grid[1]].had_catch = False
        return

    def over(self,event):
        if self.is_over_ok() or self.king_is_over_ok():
            self['image'] = CheckButton.image_dict[CheckButton.turn]['mover']
            self.overd = True
            CheckButton.overd = self.get_row_col()
            send_msg = {'method' : 'over', 'grid' : CheckButton.overd}
            client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_over(self):
        self['image'] = CheckButton.image_dict[CheckButton.turn]['mover']
        self.overd = True
        CheckButton.overd = self.get_row_col()
        return

    def is_over_ok(self):
        if (not CheckButton.ckd) or self.wbb !=2:
            return False
        p_or_m = -1 if CheckButton.turn ==0 else 1
        can_over = self.get_row_col() == (CheckButton.ckd[0] + p_or_m, CheckButton.ckd[1] -1)\
            or self.get_row_col() == (CheckButton.ckd[0] + p_or_m, CheckButton.ckd[1] +1)
        if can_over or self.can_catch():
            return True
        return False

    def king_is_over_ok(self):
        if (not CheckButton.ckd) or self.wbb !=2:
            return False
        elif not CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]].is_king:
            return False
        king_is_move = abs(self.get_row_col()[0] - CheckButton.ckd[0]) == abs(self.get_row_col()[1] - CheckButton.ckd[1])
        gap = abs(CheckButton.ckd[0] - self.get_row_col()[0])
        p_or_m_row = 1 if CheckButton.ckd[0] - self.get_row_col()[0]<0 else -1
        p_or_m_col = 1 if CheckButton.ckd[1] - self.get_row_col()[1]<0 else -1
        for i in range(1,gap):
            r = CheckButton.ckd[0] + (p_or_m_row*i)
            c = CheckButton.ckd[1] + (p_or_m_col*i)
            if (r,c) in CheckButton.grid_dict[CheckButton.turn]:
                king_is_move = False
        return king_is_move

    def over_out(self,event):
        if not self.overd:
            return
        self['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.overd = False
        CheckButton.overd = tuple()
        send_msg = {'method' : 'over_out', 'grid' : self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_over_out(self):
        self['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.overd = False
        CheckButton.overd = tuple()
        return

    def change_grid_dict(self, delete_button, append_button):
        if abs(delete_button[0] - append_button[0]) ==2:
            opp = 1 if CheckButton.turn ==0 else 0 
            opp_king = 4 if CheckButton.turn ==0 else 3
            catch_pt = int((delete_button[0]+append_button[0])/2), int((delete_button[1]+append_button[1])/2)
            CheckButton.grid_dict[opp].remove(catch_pt)
            CheckButton.grid_dict[2].append(catch_pt)
            if catch_pt in CheckButton.grid_dict[opp_king]:
                CheckButton.grid_dict[opp_king].remove(catch_pt)
        CheckButton.grid_dict[CheckButton.turn].remove(delete_button)
        CheckButton.grid_dict[2].append(delete_button)
        CheckButton.grid_dict[2].remove(append_button)
        CheckButton.grid_dict[CheckButton.turn].append(append_button)
        print(CheckButton.grid_dict)
        return
    
    def king_change_grid_dict(self, delete_button, append_button):
        gap = abs(CheckButton.ckd[0] - CheckButton.overd[0])
        p_or_m_row = 1 if CheckButton.ckd[0] - CheckButton.overd[0]<0 else -1
        p_or_m_col = 1 if CheckButton.ckd[1] - CheckButton.overd[1]<0 else -1
        opp = 1 if CheckButton.turn ==0 else 0
        opp_king = 4 if CheckButton.turn ==0 else 3
        for i in range(1,gap):
            r = CheckButton.ckd[0] + (p_or_m_row*i)
            c = CheckButton.ckd[1] + (p_or_m_col*i)
            if (r,c) in CheckButton.grid_dict[opp]:
                # 잡힌 말 속성 정리
                CheckButton.button_list[r][c].wbb =2
                CheckButton.overd = tuple()
                # 잡힌 말 이미지 정리
                CheckButton.button_list[r][c]['image'] = CheckButton.image_dict[2]['bg'][1]
                # grid_dict 정리
                CheckButton.grid_dict[opp].remove((r,c))
                CheckButton.grid_dict[2].append((r,c))
                if (r,c) in CheckButton.grid_dict[opp_king]:
                    CheckButton.button_list[r][c].is_king = ''
                    CheckButton.grid_dict[opp_king].remove((r,c))
                self.had_catch = True
        turn = 3 if CheckButton.turn ==0 else 4
        CheckButton.grid_dict[CheckButton.turn].remove(delete_button)
        CheckButton.grid_dict[turn].remove(delete_button)
        CheckButton.grid_dict[2].append(delete_button)
        CheckButton.grid_dict[2].remove(append_button)
        CheckButton.grid_dict[CheckButton.turn].append(append_button)
        CheckButton.grid_dict[turn].append(append_button)
        print(CheckButton.grid_dict)
        return

    def just_move(self):
        if abs(CheckButton.ckd[0] - self.get_row_col()[0]) ==2:
            catch_pt = int((CheckButton.ckd[0]+self.get_row_col()[0])/2), int((CheckButton.ckd[1]+self.get_row_col()[1])/2)
            catch_bt = CheckButton.button_list[catch_pt[0]][catch_pt[1]]
            catch_bt.wbb =2
            catch_bt['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
            self.had_catch = True
            if catch_bt.is_king:
                catch_bt.is_king = False
        unclick_button = CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]]
        unclick_button.wbb =2
        unclick_button.ckd = False
        unclick_button['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.wbb = CheckButton.turn
        self.overd = False
        CheckButton.ckd = tuple()
        self['image'] = CheckButton.image_dict[self.wbb]['bg']
        send_msg = {'method' : 'just_move', 'grid' : self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_just_move(self):
        if abs(CheckButton.ckd[0] - self.get_row_col()[0]) ==2:
            catch_pt = int((CheckButton.ckd[0]+self.get_row_col()[0])/2), int((CheckButton.ckd[1]+self.get_row_col()[1])/2)
            catch_bt = CheckButton.button_list[catch_pt[0]][catch_pt[1]]
            catch_bt.wbb =2
            catch_bt['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
            self.had_catch = True
            if catch_bt.is_king:
                opp_king = 4 if CheckButton.turn ==0 else 3
                catch_bt.is_king = False
                CheckButton.grid_dict[opp_king].remove(catch_pt)
            # 좌표 수정하기
            opp = 1 if CheckButton.turn ==0 else 0 
            CheckButton.grid_dict[opp].remove(catch_pt)
            CheckButton.grid_dict[2].append(catch_pt)
        # 좌표 수정하기
        CheckButton.grid_dict[CheckButton.turn].remove(CheckButton.ckd)
        CheckButton.grid_dict[2].append(CheckButton.ckd)
        CheckButton.grid_dict[2].remove(self.get_row_col())
        CheckButton.grid_dict[CheckButton.turn].append(self.get_row_col())
        # 속성 수정하기
        unclick_button = CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]]
        unclick_button.wbb =2
        unclick_button.ckd = False
        unclick_button['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.wbb = CheckButton.turn
        self.overd = False
        CheckButton.ckd = tuple()
        self['image'] = CheckButton.image_dict[self.wbb]['bg']
        print(CheckButton.grid_dict)
        return

    def recv_catch_again(self):
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        self['image'] = CheckButton.image_dict[self.wbb]['ckd']
        return
    
    def recv_king_catch_again(self):
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
        return

    def king_just_move(self):
        unclick_button = CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]]
        unclick_button.wbb =2
        unclick_button.ckd = False
        unclick_button.is_king = False
        unclick_button['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.wbb = CheckButton.turn
        self.overd = False
        self.is_king = True
        CheckButton.ckd = tuple()
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        send_msg = {'method' : 'king_just_move', 'grid' : self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        return

    def recv_king_just_move(self):
        gap = abs(CheckButton.ckd[0] - CheckButton.overd[0])
        p_or_m_row = 1 if CheckButton.ckd[0] - CheckButton.overd[0]<0 else -1
        p_or_m_col = 1 if CheckButton.ckd[1] - CheckButton.overd[1]<0 else -1
        opp = 1 if CheckButton.turn ==0 else 0
        opp_king = 4 if CheckButton.turn ==0 else 3
        for i in range(1,gap):
            r = CheckButton.ckd[0] + (p_or_m_row*i)
            c = CheckButton.ckd[1] + (p_or_m_col*i)
            if (r,c) in CheckButton.grid_dict[opp]:
                # 잡힌 말 속성 정리
                CheckButton.button_list[r][c].wbb =2
                CheckButton.overd = tuple()
                # 잡힌 말 이미지 정리
                CheckButton.button_list[r][c]['image'] = CheckButton.image_dict[2]['bg'][1]
                # grid_dict 정리
                CheckButton.grid_dict[opp].remove((r,c))
                CheckButton.grid_dict[2].append((r,c))
                if (r,c) in CheckButton.grid_dict[opp_king]:
                    CheckButton.button_list[r][c].is_king = ''
                    CheckButton.grid_dict[opp_king].remove((r,c))
            self.had_catch = True
        turn = 3 if CheckButton.turn ==0 else 4
        CheckButton.grid_dict[CheckButton.turn].remove(CheckButton.ckd)
        CheckButton.grid_dict[turn].remove(CheckButton.ckd)
        CheckButton.grid_dict[2].append(CheckButton.ckd)
        CheckButton.grid_dict[2].remove(self.get_row_col())
        CheckButton.grid_dict[CheckButton.turn].append(self.get_row_col())
        CheckButton.grid_dict[turn].append(self.get_row_col())
        unclick_button = CheckButton.button_list[CheckButton.ckd[0]][CheckButton.ckd[1]]
        unclick_button.wbb =2
        unclick_button.ckd = False
        unclick_button.is_king = False
        unclick_button['image'] = CheckButton.image_dict[2]['bg'][self.get_bg_num()]
        self.wbb = CheckButton.turn
        self.overd = False
        self.is_king = True
        CheckButton.ckd = tuple()
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        return

    def can_catch(self):
        opp = 1 if CheckButton.turn ==0 else 0
        is_two = self.get_row_col() == (CheckButton.ckd[0]-2, CheckButton.ckd[1]-2)\
             or self.get_row_col() == (CheckButton.ckd[0]-2, CheckButton.ckd[1]+2)
        is_two_back = self.get_row_col() == (CheckButton.ckd[0]+2, CheckButton.ckd[1]-2)\
             or self.get_row_col() == (CheckButton.ckd[0]+2, CheckButton.ckd[1]+2)
        middle_pt = int((self.get_row_col()[0]+CheckButton.ckd[0])/2), int((self.get_row_col()[1]+CheckButton.ckd[1])/2)
        catch_okay = (is_two or is_two_back) and middle_pt in CheckButton.grid_dict[opp]
        return catch_okay

    def can_catch_again(self):
        opp = 0 if CheckButton.turn == 1 else 1
        opp_grid = CheckButton.grid_dict[opp]
        self_grid = self.get_row_col()
        right_up = (self_grid[0]-1,self_grid[1]+1)
        right_down = (self_grid[0]+1,self_grid[1]+1)
        left_up = (self_grid[0]-1,self_grid[1]-1)
        left_down = (self_grid[0]+1,self_grid[1]-1)
        is_opp_continue = right_up in opp_grid or right_down in opp_grid\
             or left_up in opp_grid or left_down in opp_grid
        if is_opp_continue:
            right_up_blank = (right_up[0]-1,right_up[1]+1)
            right_down_blank = (right_down[0]+1,right_down[1]+1)
            left_up_blank = (left_up[0]-1,left_up[1]-1)
            left_down_blank = (left_down[0]+1,left_down[1]-1)
            catch_again = (right_up in opp_grid and right_up_blank in CheckButton.grid_dict[2])\
                 or (right_down in opp_grid and right_down_blank in CheckButton.grid_dict[2])\
                      or (left_up in opp_grid and left_up_blank in CheckButton.grid_dict[2])\
                           or (left_down in opp_grid and left_down_blank in CheckButton.grid_dict[2]) 
        else:
            return False
        return catch_again

    def king_can_catch_again(self):
        r = self.get_row_col()[0]
        c = self.get_row_col()[1]
        left_up_cnt = r if r<=c else c
        right_up_cnt = r if r<=9-c else 9-c
        left_down_cnt = 9-r if 9-r<=c else c
        right_down_cnt = 9-r if 9-r<=9-c else 9-c
        cnt = [left_up_cnt, right_up_cnt, left_down_cnt, right_down_cnt]
        opp = 1 if CheckButton.turn ==0 else 0
        # left_up
        for i in range(1, left_up_cnt+1):
            opp_button = (r-i,c-i)
            blank = (r-i-1,c-i-1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # right_up
        for i in range(1, right_up_cnt+1):
            opp_button = (r-i,c+i)
            blank = (r-i-1,c+i+1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # left_down
        for i in range(1, left_down_cnt+1):
            opp_button = (r+i,c-i)
            blank = (r+i+1,c-i-1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        # right_down
        for i in range(1, right_down_cnt+1):
            opp_button = (r+i,c+i)
            blank = (r+i+1,c+i+1)
            if opp_button in CheckButton.grid_dict[CheckButton.turn]:
                break
            elif opp_button in CheckButton.grid_dict[opp] and blank in CheckButton.grid_dict[2]:
                return True
            else:
                continue
        return

    def make_king(self):
        self.is_king = True
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        turn = 3 if CheckButton.turn ==0 else 4
        CheckButton.grid_dict[turn].append(self.get_row_col())
        send_msg = {'method' : 'make_king', 'grid' : self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        time.sleep(0.05)
        return

    def recv_make_king(self):
        self.is_king = True
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        turn = 3 if CheckButton.turn ==0 else 4
        CheckButton.grid_dict[turn].append(self.get_row_col())
        return

    def check_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['ckd']
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        send_msg = {'method' : 'check_ckd', 'grid' : CheckButton.ckd}
        client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_check_ckd(self):
        print(self.get_row_col())
        self['image'] = CheckButton.image_dict[self.wbb]['ckd']
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        return

    def uncheck_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['bg']
        self.ckd = False
        CheckButton.ckd = ()
        send_msg = {'method' : 'uncheck_ckd', 'grid': self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        return

    def recv_uncheck_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['bg']
        self.ckd = False
        CheckButton.ckd = ()
        return
    
    def king_check_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        send_msg = {'method' : 'king_check_ckd', 'grid' : CheckButton.ckd}
        client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_king_check_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['king_ckd']
        self.ckd = True
        CheckButton.ckd = self.get_row_col()
        return
    
    def king_uncheck_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        self.ckd = False
        CheckButton.ckd = tuple()
        send_msg = {'method' : 'king_uncheck_ckd', 'grid' : self.get_row_col()}
        client_socket.send(json.dumps(send_msg).encode())
        return
    
    def recv_king_uncheck_ckd(self):
        self['image'] = CheckButton.image_dict[self.wbb]['king']
        self.ckd = False
        CheckButton.ckd = tuple()
        return

    def get_row_col(self):
        grid_info = self.grid_info()
        row, col = grid_info['row'], grid_info['column']
        return row, col
    
    def get_bg_num(self):
        row, col = self.get_row_col()
        return (row+col)%2
    
    @staticmethod
    def undo(event):
        if CheckButton.turn != CheckButton.my_turn:
            return
        if len(CheckButton.history) ==1 or len(CheckButton.history) ==2:
            print("cannot ctrl + z")
        else:
            CheckButton.grid_dict = copy.deepcopy(CheckButton.history[-2].grid)
            CheckButton.turn = CheckButton.history[-2].turn
            CheckButton.ckd = CheckButton.history[-2].ckd
            CheckButton.button_list = []
            CheckButton.make_button()
            if CheckButton.grid_dict[3]:
                for button in CheckButton.grid_dict[3]:
                    CheckButton.button_list[button[0]][button[1]].is_king = True
            if CheckButton.grid_dict[4]:
                for button in CheckButton.grid_dict[4]:
                    CheckButton.button_list[button[0]][button[1]].is_king = True
            if CheckButton.ckd:
                for i in CheckButton.grid_dict[CheckButton.turn]:
                    if i == CheckButton.ckd:
                        CheckButton.button_list[i[0]][i[1]].ckd = True
                        CheckButton.button_list[i[0]][i[1]].had_catch = True
                        if CheckButton.button_list[i[0]][i[1]].is_king:
                            CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['king_ckd']
                        else:
                            CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['ckd']

            send_msg = {'method' : 'undo', 'history' : CheckButton.history[-2]}
            client_socket.send(json.dumps(send_msg).encode())

            CheckButton.history.remove(CheckButton.history[-1])
        return
    
    @staticmethod
    def recv_undo(h_list):
        CheckButton.grid_dict = copy.deepcopy(h_list.grid)
        CheckButton.turn = h_list.turn
        CheckButton.ckd = h_list.ckd
        CheckButton.button_list = []
        CheckButton.make_button()
        if CheckButton.grid_dict[3]:
            for button in CheckButton.grid_dict[3]:
                CheckButton.button_list[button[0]][button[1]].is_king = True
        if CheckButton.grid_dict[4]:
            for button in CheckButton.grid_dict[4]:
                CheckButton.button_list[button[0]][button[1]].is_king = True
        if CheckButton.ckd:
            for i in CheckButton.grid_dict[CheckButton.turn]:
                if i == CheckButton.ckd:
                    CheckButton.button_list[i[0]][i[1]].ckd = True
                    CheckButton.button_list[i[0]][i[1]].had_catch = True
                    if CheckButton.button_list[i[0]][i[1]].is_king:
                        CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['king_ckd']
                    else:
                        CheckButton.button_list[i[0]][i[1]]['image'] = CheckButton.image_dict[CheckButton.turn]['ckd']

    @classmethod
    def make_grid_dict(cls):
        for i in range(4):
            for j in range(10):
                if i%2==0 and j%2!=0:
                    CheckButton.black_grid.append((i,j))
                elif i%2!=0 and j%2==0:
                    CheckButton.black_grid.append((i,j))
        for i in range(6,10):
            for j in range(10):
                if i%2==0 and j%2!=0:
                    CheckButton.white_grid.append((i,j))
                elif i%2!=0 and j%2==0:
                    CheckButton.white_grid.append((i,j))

        cls.grid_dict = {
            0 : CheckButton.white_grid,
            1 : CheckButton.black_grid,
            2 : list(set([(i, j) for i in range(10) for j in range(10)]) - set(CheckButton.white_grid +CheckButton.black_grid)),
            3:[],
            4:[]
        }

        cls.history = [History(copy.deepcopy(cls.grid_dict), CheckButton.turn, tuple())]
    
    @classmethod
    def make_button(cls):
        item = list(CheckButton.grid_dict.items())[0:3]
        for i in range(10):
            cls.button_list.append([])
            for j in range(10):
                position = (i, j)
                for wbb, pts in item:
                    if position in pts:
                        #책버튼으로 인스턴스를 생성했다. 그 위치는 [i][j]에 있다.
                        if position in CheckButton.grid_dict[3]:
                            cls.button_list[i].append(cls(root, wbb=0, image=cls.image_dict[0]['king']))  
                        elif position in CheckButton.grid_dict[4]:
                            cls.button_list[i].append(cls(root, wbb=1, image=cls.image_dict[1]['king']))
                        elif position in CheckButton.grid_dict[2]:
                            cls.button_list[i].append(cls(root, wbb=wbb, image=cls.image_dict[wbb]['bg'][(i+j)%2]))
                        else:
                            cls.button_list[i].append(cls(root, wbb=wbb, image=cls.image_dict[wbb]['bg']))
                        cls.button_list[i][j].grid(row=i, column=j)
                        cls.button_list[i][j]['command'] = cls.button_list[i][j].click


CheckButton.image_dict={
    0:{
    'bg' : [PhotoImage(file=path + "/checker_folder/white_bg1.png")],
    'ckd' : PhotoImage(file=path + "/checker_folder/white_ckd.png"),
    'mover' : PhotoImage(file=path + "/checker_folder/white_mover.png"),
    'king' : PhotoImage(file=path + "/checker_folder/white_king.png"),
    'king_ckd' : PhotoImage(file=path + "/checker_folder/white_king_ckd.png")
    },
    1:{
    'bg' : [PhotoImage(file=path + "/checker_folder/black_bg1.png")],
    'ckd' : PhotoImage(file=path + "/checker_folder/black_ckd.png"),
    'mover' : PhotoImage(file=path + "/checker_folder/black_mover.png"),
    'king' : PhotoImage(file=path + "/checker_folder/black_king.png"),
    'king_ckd' : PhotoImage(file=path + "/checker_folder/black_king_ckd.png")
    },
    2:{
    'bg' : [PhotoImage(file=path + "/checker_folder/blank_bg0.png"), PhotoImage(file=path + "/checker_folder/blank_bg1.png")]
    }}

CheckButton.make_grid_dict()
CheckButton.make_button()

root.bind("<Control - z>", CheckButton.undo)

if __name__=='__main__':
    root.mainloop()

print(dir(CheckButton))